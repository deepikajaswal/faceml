
%% Using Image from database
%   videoFrame = imread('faces1.jpg');

%% Using Image from webcam.
cam = webcam();
videoFrame = snapshot(cam);

%% Create the face detector object.
faceDetector = vision.CascadeObjectDetector();

%use obj on image
bBox = step(faceDetector, videoFrame);

%mark faces on image
o = insertObjectAnnotation(videoFrame, 'rectangle', bBox, 'face');
imshow(o);

s=size(bBox,1);
n = 1;

while n <= s 
    faceImg = imcrop(o,bBox(n,:));
    str_num = num2str(n);
    str = strcat('Person: ', str_num);
    figure; imshow(faceImg);title(str);
    n = n +1;
end

clear cam;
release(faceDetector);